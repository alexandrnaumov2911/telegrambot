#for_gitlab
from config import TOKEN

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.utils import executor


bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands="start")
async def cmd_start(msg: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Ольга Михайловна",
               "Дарья Халявина",
               "Анастасия Александрова",
               "Татьяна Пасынкова",
               "Софья Пипкина",
               "Влада Масленникова",
               "Дарья Лукина",
               "Ирина Муравьёва",
        ]
    keyboard.add(*buttons)
    await msg.answer(f"""Девочки❤️, мы поздравляем вас с праздником.
Для каждой из вас подготовлено индивидуальное поздравление.
Нажмите на кнопку с вашим именем и бла блалалалала)""", reply_markup=keyboard)


@dp.message_handler(Text(equals="Ирина Муравьёва"))
async def Irina(msg: types.message):
    with open('Irina.mp4', 'rb') as Irina:
        await bot.send_video_note(msg.from_user.id, Irina)


@dp.message_handler(Text(equals="Ольга Михайловна"))
async def Olga(msg: types.message):
    with open('Olga.mp4', 'rb') as Olga:
        await bot.send_video_note(msg.from_user.id, Olga)


@dp.message_handler(Text(equals="Дарья Халявина"))
async def DariaH(msg: types.message):
    with open('DariaH.mp4', 'rb') as DariaH:
        await bot.send_video_note(msg.from_user.id, DariaH)


@dp.message_handler(Text(equals="Анастасия Александрова"))
async def Anastasia(msg: types.message):
    with open('Anastasia.mp4', 'rb') as Anastasia:
        await bot.send_video_note(msg.from_user.id, Anastasia)


@dp.message_handler(Text(equals="Татьяна Пасынкова"))
async def Tatyana(msg: types.message):
    with open('Tatyana.mp4', 'rb') as Tatyana:
        await bot.send_video_note(msg.from_user.id, Tatyana)


@dp.message_handler(Text(equals="Софья Пипкина"))
async def Sofia(msg: types.message):
    with open('Sofia.mp4', 'rb') as Sofia:
        await bot.send_video(msg.from_user.id, Sofia)
        await bot.send_message(msg.from_user.id, f'Слушать в наушниках !!')


@dp.message_handler(Text(equals="Влада Масленникова"))
async def Vlada(msg: types.message):
    with open('Vlada.mp4', 'rb') as Vlada:
        await bot.send_video_note(msg.from_user.id, Vlada)


@dp.message_handler(Text(equals="Дарья Лукина"))
async def DariaL(msg: types.message):
    with open('DariaL.mp4', 'rb') as DariaL:
        await bot.send_video_note(msg.from_user.id, DariaL)




if __name__ == '__main__':
    executor.start_polling(dp)

